import { Component, OnInit, ViewChild } from '@angular/core';
import { NotesdbService } from '../notesdb.service';
import { NoteItem } from '../../noteitem';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.page.html',
  styleUrls: ['./notebook.page.scss'],
})
export class NotebookPage implements OnInit {
  noteCount: number;
  notes = Array<NoteItem>();
  noteWork = Array<NoteItem>();
  noteMeeting = Array<NoteItem>();
  noteBrain = Array<NoteItem>();
  newNote: string;
  newNoteWork: string;
  newNoteMeeting: string;
  newNoteBrain: string;

  @ViewChild('myInput', null) myInput;


  constructor(private notesDB: NotesdbService) { }

  ngOnInit() {
    this.notesDB.getAll()
      .then(data => {
        this.notes = JSON.parse(String(data));
        if (!this.notes) {
          this.notes = [];
        }
        this.noteCount = this.notes.length;
      },
      err => {
        console.log(err);
      });
  }

  addNote(event) {
    if (event.keyCode === 13) {
      const newNoteitem: NoteItem = new NoteItem();
      newNoteitem.name = this.newNote;
      this.notes.push(newNoteitem);
      this.newNote = '';
      this.noteCount = this.notes.length;
      this.notesDB.save(this.notes);
    }
  }

  addNoteWork(event) {
    if (event.keyCode === 13) {
      const newNoteWorkitem: NoteItem = new NoteItem();
      newNoteWorkitem.name = this.newNoteWork;
      this.notes.push(newNoteWorkitem);
      this.newNoteWork = '';
      this.noteCount = this.notes.length;
      this.notesDB.save(this.notes);
    }
  }

  addNoteMeeting(event) {
    if (event.keyCode === 13) {
      const newNoteMeetingitem: NoteItem = new NoteItem();
      newNoteMeetingitem.name = this.newNoteMeeting;
      this.notes.push(newNoteMeetingitem);
      this.newNoteMeeting = '';
      this.noteCount = this.notes.length;
      this.notesDB.save(this.notes);
    }
  }

  addNoteBrain(event) {
    if (event.keyCode === 13) {
      const newNoteBrainitem: NoteItem = new NoteItem();
      newNoteBrainitem.name = this.newNoteBrain;
      this.notes.push(newNoteBrainitem);
      this.newNoteBrain = '';
      this.noteCount = this.notes.length;
      this.notesDB.save(this.notes);
    }
  }

removeNote(index) {
  this.notes.splice(index,1);
  this.notesDB.save(this.notes);

}

removeNoteWork(index) {
  this.notes.splice(index,1);
  this.notesDB.save(this.notes);

}

removeNoteMeeting(index) {
  this.notes.splice(index,1);
  this.notesDB.save(this.notes);

}

removeNoteBrain(index) {
  this.notes.splice(index,1);
  this.notesDB.save(this.notes);

}

}
