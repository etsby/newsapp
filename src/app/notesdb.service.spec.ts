import { TestBed } from '@angular/core/testing';

import { NotesdbService } from './notesdb.service';

describe('NotesdbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotesdbService = TestBed.get(NotesdbService);
    expect(service).toBeTruthy();
  });
});
