import { TestBed } from '@angular/core/testing';

import { EventsdbService } from './eventsdb.service';

describe('EventsdbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsdbService = TestBed.get(EventsdbService);
    expect(service).toBeTruthy();
  });
});
