import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  items: any;
  keyword: string;

  constructor(
    private newsService: NewsService,
    private router: Router) {

     }

  ngOnInit() {
    this.newsService
    .getData('top-headlines?country=us')
    .subscribe(data => {
      this.items = data;
    });
  }

  business() {
    this.newsService
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      this.items = data;
    });
  }

  entertainment() {
    this.newsService
    .getData('top-headlines?country=us&category=entertainment')
    .subscribe(data => {
      this.items = data;
    });
  }

  general() {
    this.newsService
    .getData('top-headlines?country=us&category=general')
    .subscribe(data => {
      this.items = data;
    });
  }

  health() {
    this.newsService
    .getData('top-headlines?country=us&category=health')
    .subscribe(data => {
      this.items = data;
    });
  }

  science() {
    this.newsService
    .getData('top-headlines?country=us&category=science')
    .subscribe(data => {
      this.items = data;
    });
  }

  sport() {
    this.newsService
    .getData('top-headlines?country=us&category=sport')
    .subscribe(data => {
      this.items = data;
    });
  }

  technology() {
    this.newsService
    .getData('top-headlines?country=us&category=technology')
    .subscribe(data => {
      this.items = data;
    });
  }


  search() {
    this.newsService
    .getData('top-headlines?q=' + this.keyword + '')
    .subscribe(data => {
      console.log(data);
      this.items = data;
    });
  }
}
