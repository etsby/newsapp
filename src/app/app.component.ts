import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'News',
      url: '/home',
      icon: 'newspaper'
    },


    {
      title: 'Calendar',
      url: '/calendar',
      icon: 'calendar'
    },
    
    {
      title: 'Notebook',
      url: '/notebook',
      icon: 'book'
    },

    {
      title: 'About',
      url: '/about',
      icon: 'information'
    },

    {
      title: 'News2',
      url: '/news',
      icon: 'newspaper'
      
    },

    
  ];

  
  

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
