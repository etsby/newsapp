import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  getData(arg0: string) {
    throw new Error("Method not implemented.");
  }
  API_KEY = '71d5cd04d03e4c01849281973e94b511';

  constructor(private httpClient: HttpClient) { }
  getNews(){
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
  }
}