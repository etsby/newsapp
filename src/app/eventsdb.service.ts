import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventsdbService {

  constructor(private storage: Storage) { }

  getAll(){
    return new Promise((resolve, reject) => {
      this.storage.get('events').then(data => {
        resolve(data);
      },
      error => {
        reject('Error retrieving data from storage');
      });
    });
  }

  save(events: Array<Event>) {
    this.storage.set('items', JSON.stringify(events));
  }
}
