import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { resolve } from 'url';
import { NoteItem } from 'src/noteitem';

@Injectable({
  providedIn: 'root'
})
export class NotesdbService {

  constructor(private storage: Storage) { }

  getAll() {
    return new Promise((resolve, reject) => {
      this.storage.get('notes').then(data => {
        resolve(data);
      },
      error => {
        reject('Error retrieving data from storage');
      });
    });
  }

  save(items: Array<NoteItem>) {
    this.storage.set('notes', JSON.stringify(items));
  }

}
